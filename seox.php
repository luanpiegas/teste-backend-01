<?php
/*
Plugin Name: Seox
*/ 

// Register custom taxonomy 'Regiões' with slug 'regioes'
function seox_register_regioes_taxonomy() {
  $labels = [
    'name' => _x( 'Regiões', 'taxonomy general name' ),
    'singular_name' => _x( 'Região', 'taxonomy singular name' ),
    'search_items' =>  __( 'Buscar Regiões' ),
    'all_items' => __( 'Todas as Regiões' ),
    'edit_item' => __( 'Editar Região' ), 
    'update_item' => __( 'Atualizar Região' ),
    'add_new_item' => __( 'Adicionar nova Região' ),
    'menu_name' => __( 'Regiões' ),
  ];    
 
  $args = [
    'hierarchical' => true,
    'labels' => $labels,
    'rewrite' => [ 'slug' => 'regioes' ],
  ];
 
  register_taxonomy( 'regioes', [ 'post' ], $args );
}
add_action( 'init', 'seox_register_regioes_taxonomy' );

// Rewrite URLs to add custom taxonomy at beginning of path with 'noticias' slug
function seox_add_noticias_to_url( $link, $post ) {
  if ( 'post' === $post->post_type ) {
    $terms = get_the_terms( $post->ID, 'regioes' );
    var_dump($terms);
    if ( $terms && ! is_wp_error( $terms ) ) {
      $term_slug = array_pop( $terms )->slug;
      $link = home_url( $terms[1]->slug . '/' . $terms[0]->slug . '/noticias/' . $post->post_name . '/' );
    }
  }
  return $link;
}
add_filter( 'post_link', 'seox_add_noticias_to_url', 10, 2 );

// Reverse rewrite for post/page without the custom taxonomy
function seox_remove_noticias_from_url( $link, $post ) {
  if ( 'post' === $post->post_type ) {
    $terms = get_the_terms( $post->ID, 'regioes' );
    if ( ! $terms || is_wp_error( $terms ) ) {
      $link = home_url( $post->post_name . '/' );
    }
  }
  return $link;
}
add_filter( 'post_type_link', 'seox_remove_noticias_from_url', 10, 2 );

// Create REST endpoint to return all information for a specific value of this taxonomy created
function seox_get_regioes_data( $data ) {
  $args = [
    'post_type' => 'post',
    'tax_query' => [
      [
        'taxonomy' => 'regioes',
        'field' => 'slug',
        'terms' => $data['slug'],
      ],
    ],
  ];
  $query = new WP_Query( $args );
  return $query->posts;
}
add_action( 'rest_api_init', function() {
  register_rest_route( 'regioes/v1', '/regioes/(?P<slug>[a-zA-Z0-9-]+)', [
    'methods' => 'GET',
    'callback' => 'seox_get_regioes_data',
  ] );
} );